﻿using System;
using System.Diagnostics;
using System.Threading;

/** 
Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps 
through the list to be sorted, compares each pair of adjacent items and swaps them if they are in the wrong 
order. The pass through the list is repeated until no swaps are needed, which indicates that the list is sorted.
The algorithm, which is a comparison sort, is named for the way smaller elements "bubble" to the top of the list.
Although the algorithm is simple, it is too slow and impractical for most problems even when compared to insertion
sort.[1] It can be practical if the input is usually in sorted order but may occasionally have some out-of-order 
elements nearly in position.

	Array
Worst case performance		O(n^{2})
Best case performance		O(n)
Average case performance 	O(n^{2})
**/

namespace BubbleSort
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int arraySize = 0;
			try
			{
				Console.WriteLine("Press 1 for custom values and 2 for randomly generated values");
				int input = int.Parse(Console.ReadLine());
				switch(input)
				{
				case 1: 
					Console.WriteLine ("Enter size of your array:");
					arraySize = int.Parse(Console.ReadLine ());
					double[] unsortedArray = new double[arraySize+1];
					Console.WriteLine ("Enter elements of the array one by one pressing enter after every element");
					for(int i =0;i<arraySize;i++)
					{
						unsortedArray [i] = double.Parse(Console.ReadLine ());
					}
					BubbleSort (unsortedArray, arraySize);
					break;

				case 2:
					Console.WriteLine("Enter number of array element to generate");
					StressTester(int.Parse(Console.ReadLine()));
					break;
				}
			}
			catch(Exception e)
			{
				Console.WriteLine ("ERROR : \t {0}", e);
			}
		}

		public static void BubbleSort(double[] unsortedArray, double arraySize)
		{
			var stopWatch = new Stopwatch ();
			stopWatch.Start ();
			for(int i=0;i<arraySize;i++)
			{
				for(int j=0; j<arraySize-1; j++)
				{
					double temp = 0;
					if(unsortedArray[j] > unsortedArray[j+1])
					{
						//Swapping
						temp = unsortedArray [j];
						unsortedArray [j] = unsortedArray [j + 1];
						unsortedArray [j + 1] = temp;
					}
				}
			}

			//Printing Sorted Array
			Console.WriteLine ();
			for(int i =0; i<arraySize; i++)
			{				
				Console.Write (unsortedArray [i] + "|");
			}
			Console.WriteLine ("\n");
			stopWatch.Stop ();
			Console.WriteLine ("Time Elapsed in sorting : {0}",stopWatch.Elapsed);
		}

		public static void StressTester(int arrayElements)
		{
			double[] unsortedArray = new double[arrayElements];
			Random random = new Random ();
			Console.WriteLine ("Generating random numbers..");
			for(int i=0; i<arrayElements;i++)
			{
				Console.Write (".");
				unsortedArray [i] = random.NextDouble() * random.Next(-10000,10000);
				Console.Write("\b");
			}
			BubbleSort (unsortedArray, arrayElements);
		}
	}
}
