﻿using System;
using System.Diagnostics;

/** 
Insertion sort is a simple sorting algorithm that builds the final sorted array (or list) one item at a time.
It is much less efficient on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort.
However, insertion sort provides several advantages:


- Simple implementation: Bentley shows a three-line C version, and a five-line optimized version[1]:116
- Efficient for (quite) small data sets, much like other quadratic sorting algorithms
- More efficient in practice than most other simple quadratic (i.e., O(n2)) algorithms such as selection sort or bubble sort
- Adaptive, i.e., efficient for data sets that are already substantially sorted: the time complexity is O(nk) when each element in the input is no more than k places away from its sorted position
- Stable; i.e., does not change the relative order of elements with equal keys
- In-place; i.e., only requires a constant amount O(1) of additional memory space
- Online; i.e., can sort a list as it receives it


Worst case performance	О(n^2) comparisons, swaps
Best case performance	O(n) comparisons, O(1) swaps
Average case performance	О(n^2) comparisons, swaps
Worst case space complexity	О(n) total, O(1) auxiliary
**/

namespace InsertionSort
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Press 1 to manually input values or 2 to randomly generate values in array");
			switch(int.Parse(Console.ReadLine()))
			{
			case 1:
				Console.WriteLine ("Enter the desired size for the array");
				int arraySize = int.Parse (Console.ReadLine ());
				double[] unsortedArray = new double[arraySize];
				Console.WriteLine ("Enter the elements of the array ony by one and press enter after entering each element");
				for (int i = 0; i < arraySize; i++) {
					unsortedArray [i] = double.Parse (Console.ReadLine ());
				}
				InsertionSort (unsortedArray, arraySize);
				break;
			case 2: 
				Console.WriteLine ("Enter number of elements to generate!");
				StressTester (int.Parse (Console.ReadLine ()));
				break;
			}
		}

		public static void InsertionSort(double[] unsortedArray, int arraySize)
		{
			Stopwatch stopwatch = new Stopwatch ();
			stopwatch.Start ();

			/** 
			  Pseudo-Code
			  
			  	for i ← 1 to length(A)-1
    				j ← i
    				while j > 0 and A[j-1] > A[j]
        				swap A[j] and A[j-1]
        				j ← j - 1
    				end while
				end for
			**/
			for(int i=1; i<=arraySize-1;i++)
			{
				for(int j=i;j>0;j--)
				{
					if(unsortedArray[j-1] > unsortedArray[j])
					{
						var temp = unsortedArray [j-1];
						unsortedArray [j-1] = unsortedArray [j];
						unsortedArray [j] = temp;
					}
				}
			}

			Console.WriteLine ();
			for(int i=0;i<arraySize;i++)
			{
				Console.Write ("|" + unsortedArray [i]);
			}
			Console.WriteLine ();
			stopwatch.Stop ();
			Console.WriteLine ("Time Elapsed in sorting: {0}", stopwatch.Elapsed);
		}

		public static void StressTester(int arraySize)
		{
			double[] unsortedArray = new double[arraySize];
			Random random = new Random ();
			Console.WriteLine ("Generating Random Numbers!");
			for(int i=0; i<arraySize;i++)
			{
				Console.Write (".");
				unsortedArray [i] = random.NextDouble () * random.Next (-10000, 10000);
				//ASCII backspace
				Console.Write ("\b");
			}
			InsertionSort (unsortedArray, arraySize);
		}
	}
}
