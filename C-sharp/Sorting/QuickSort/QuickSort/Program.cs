﻿using System;
using System.Diagnostics;


/**
Quicksort (sometimes called partition-exchange sort) is an efficient sorting algorithm, serving as a systematic
method for placing the elements of an array in order. Developed by Tony Hoare in 1959,[1] with his work published
in 1961,[2] it is still a commonly used algorithm for sorting. When implemented well, it can be about two or three
times faster than its main competitors, merge sort and heapsort.[3]

Quicksort is a comparison sort, meaning that it can sort items of any type for which a "less-than" relation 
(formally, a total order) is defined. In efficient implementations it is not a stable sort, meaning that the 
relative order of equal sort items is not preserved. Quicksort can operate in-place on an array, requiring small 
additional amounts of memory to perform the sorting.

Mathematical analysis of quicksort shows that, on average, the algorithm takes O(n log n) comparisons to sort n 
items. In the worst case, it makes O(n2) comparisons, though this behavior is rare.



Worst case performance	O(n2)

Best case performance	O(n log n) (simple partition)
	or O(n) (three-way partition and equal keys)

Average case performance	O(n log n)

Worst case space complexity	O(n) auxiliary (naive)
	O(log n) auxiliary (Sedgewick 1978)


 **/


namespace QuickSort
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Enter size of array");
			int arraySize = int.Parse(Console.ReadLine ());
			double[] unsortedArray = new double[arraySize];

			Console.WriteLine ("Press 1 to manually input the values in array and 2 to randomly generate array elements");
			Stopwatch stopwatch = new Stopwatch ();
			stopwatch.Start ();
			switch(int.Parse(Console.ReadLine()))
			{
			case 1:
				Console.WriteLine ("Enter elements one by one and press enter after each entry.");
				for(int i=0;i<arraySize;i++)
				{
					unsortedArray [i] = double.Parse(Console.ReadLine ());
				}
				QuickSort (unsortedArray, 0, arraySize - 1);
				break;
			case 2:
				break;
			}

			for(int i =0; i<arraySize; i++)
			{
				Console.WriteLine ("|" + unsortedArray[i]);
			}

			stopwatch.Stop ();
			Console.WriteLine ("Total time elapsed in sorting: {0}", stopwatch.Elapsed);
		}

		public static void QuickSort(double[] subArray, int firstElement, int lastElement)
		{			
			if(firstElement < lastElement)
			{
				int wall = Divider (subArray, firstElement, lastElement);
				QuickSort (subArray, firstElement, wall);
				QuickSort (subArray, wall+1, lastElement);
			}
		}

		private static int Divider(double[] undividedArray, int firstElement, int lastElement)
		{
			double pivot = undividedArray [lastElement];
			int wall = firstElement;
			double temp = 0;
			for(int i=firstElement; i<lastElement-1; i++)
			{
				if(pivot >= undividedArray[i])
				{
					temp = undividedArray [wall];
					undividedArray [wall] = undividedArray [i];
					undividedArray [i] = temp;
					wall++;
				}
			}
			temp = undividedArray[lastElement];
			undividedArray [lastElement] = undividedArray [wall];
			undividedArray [wall] = temp;
			return wall;
		}

		public static void StressTester(int arraySize)
		{
			
		}
	}
}
